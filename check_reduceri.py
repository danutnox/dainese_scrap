from bs4 import BeautifulSoup
import requests
import smtplib
from datetime import datetime
import os, pwd
from email.mime.multipart import MIMEMultipart
import re
from email.mime.text import MIMEText

MAILER_URL = 'sender_mail'
password = 'sender_pw'

smtp_server = "smtp.gmail.com"
port = 587

backpack_link = 'https://www.dainese.com/on/demandware.store/Sites-Dainese_ROW-Site/en/Product-Variation?pid=201980074&dwvar_201980074_size=F001&dwvar_201980074_color=BLACK%2fRED'
pants_link = 'https://www.dainese.com/on/demandware.store/Sites-Dainese_ROW-Site/en/Product-Variation?pid=201915942&dwvar_201915942_size=B4006&dwvar_201915942_color=BLACK%2fFLUO-YELLOW'
bluza_link = 'https://www.dainese.com/on/demandware.store/Sites-Dainese_ROW-Site/en/Product-Variation?pid=201915929&dwvar_201915929_size=B4006&dwvar_201915929_color=BLACK%2fFLUO-YELLOW'
produse = ["Ghiozdanul", "Bluza", "Pantalonii"]


def currentUser():
    id = os.getuid()
    return pwd.getpwuid(id)[0]


def backpack():
    f = requests.get('https://www.dainese.com/on/demandware.store/Sites-Dainese_ROW-Site/en/Product-Variation?'
                     'pid=201980074&dwvar_201980074_size=F001&dwvar_201980074_color=BLACK%2fRED')
    f = f.text
    link = BeautifulSoup(f, 'html.parser')

    pret = link.find_all('div', attrs={'class': 'product-price'})[0].find_all(['span'])[0].get_text()
    pret = pret.replace('€ ', '').strip()
    pret = pret.replace(',', '.')
    pret = float(pret)
    return pret


def pants():
    f = requests.get('https://www.dainese.com/on/demandware.store/Sites-Dainese_ROW-Site/en/Product-Variation?'
                     'pid=201915942&dwvar_201915942_size=B4006&dwvar_201915942_color=BLACK%2fFLUO-YELLOW')
    f = f.text
    link = BeautifulSoup(f, 'html.parser')

    pret = link.find_all('div', attrs={'class': 'product-price'})[0].find_all(['span'])[0].get_text()
    pret = pret.replace('€ ', '').strip()
    pret = pret.replace(',', '.')
    pret = float(pret)
    return pret


def bluza():
    f = requests.get('https://www.dainese.com/on/demandware.store/Sites-Dainese_ROW-Site/en/Product-Variation?'
                     'pid=201915929&dwvar_201915929_size=B4006&dwvar_201915929_color=BLACK%2fFLUO-YELLOW')
    f = f.text
    link = BeautifulSoup(f, 'html.parser')

    pret = link.find_all('div', attrs={'class': 'product-price'})[0].find_all(['span'])[0].get_text()
    pret = pret.replace('€ ', '').strip()
    pret = pret.replace(',', '.')
    pret = float(pret)
    return pret


def send_mail(produs, produs_link):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.connect('smtp.gmail.com', 587)
    msg = "Salut! {} de la dainese este la reducere, fugi!\n {}".format(produs, produs_link)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(MAILER_URL, password)
    server.sendmail(MAILER_URL, 'cine_primeste_mail', msg)
    server.quit()


def logs(text):

    f = open("/home/{}/Desktop/logs.txt".format(currentUser()), 'a+')
    f.write(str(datetime.now()) + ":  " + str(text))
    f.close()


rucsac_reducere = "Rucsacul e la reducere! costa: {} euro \n".format(backpack())
rucsac_neredus = "Rucsacul nu este redus, costa: {} euro\n".format(backpack())
pants_reducere = "Pantalonii sunt redusi, costa {} euro. \n".format(pants())
pants_neredusi = "Pantalonii nu sunt redusi, costa: euro{}\n".format(pants())
bluza_redusa = "Bluza este la reducere! Costa: {} euro\n".format(bluza())
bluza_neredusa = "Bluza nu este redusa, are acelasi pret de {} euro.\n".format(bluza())

try:
    if backpack() < 84.95:
        logs(rucsac_reducere)
        send_mail(produse[0], backpack_link)
    else:
        logs(rucsac_neredus)

    if pants() < 45.95:
        logs(pants_reducere)
        send_mail(produse[2], pants_link)
    else:
        logs(pants_neredusi)

    if bluza() < 49.95:
        logs(bluza_redusa)
        send_mail(produse[1], bluza_link)
    else:
        logs(bluza_neredusa)
except Exception:
    logs(Exception)

